# frozen_string_literal: true

Gem::Specification.new do |spec|
  spec.name          = "gaycookie.dev-theme"
  spec.version       = "0.1.0"
  spec.authors       = ["GayCookie"]
  spec.email         = ["contact@gaycookie.dev"]

  spec.summary       = "Jekyll theme for my personal Jekyll website."
  spec.homepage      = "https://gitlab.com/gaycookie/gaycookie.dev-theme"
  spec.license       = "WTFPL"

  spec.files         = `git ls-files -z`.split("\x0").select { |f| f.match(%r!^(assets|_data|_layouts|_includes|_sass|LICENSE|README|_config\.yml)!i) }

  spec.add_runtime_dependency "jekyll", "~> 4.3"
end
